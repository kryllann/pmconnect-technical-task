<?php

namespace App\Http\Controllers;

use App\Services\SubscriptionService;
use App\Traits\SubscriptionValidator;
use Illuminate\Http\Request;

class SubscriptionController extends Controller
{
    use SubscriptionValidator;

    protected $subscriptionService;

    /**
     * SubscriptionController constructor.
     * @param SubscriptionService $subscriptionService
     */
    public function __construct(SubscriptionService $subscriptionService)
    {
        $this->subscriptionService = $subscriptionService;
    }

    /**
     * @param string $msisdn
     * @return \App\Subscription|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Database\Eloquent\Collection|\Illuminate\Http\Response|null
     */
    public function find(string $msisdn)
    {
        if (!$this->isMsisdnValid($msisdn)) {
            return response()->json(['error' => 'Invalid parameters']);
        }
        if ($subscription = $this->subscriptionService->find($msisdn)) {
            return $subscription;
        }
        return response()->json(['error' => 'Subscription not found']);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function subscribe(Request $request)
    {
        if ($this->isMsisdnValid($request->msisdn) && $this->isProductIdValid($request->product_id)) {
            if ($this->subscriptionService->subscribe($request->msisdn, $request->product_id)) {
                return response()->json(['message' => 'Subscribed']);
            }
            return response()->json(['error' => 'Cannot add Subscription']);

        }
        return response()->json(['error' => 'Invalid parameters']);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function unsubscribe(Request $request)
    {
        if ($this->isMsisdnValid($request->msisdn)) {
            if (isset($request->product_id)) {
                if (!$this->isProductIdValid($request->product_id)) {
                    return response()->json(['error' => 'Invalid parameters']);
                }
            }
            if ($this->subscriptionService->unsubscribe($request->msisdn, $request->product_id)) {
                return response()->json(['message' => 'Unsubscribed']);
            }
            return response()->json(['error' => 'Subscription not found']);
        }
        return response()->json(['error' => 'Invalid parameters']);
    }
}
