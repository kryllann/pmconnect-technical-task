<?php

namespace App\Traits;

trait SubscriptionValidator {

    public function isMsisdnValid(?string $msisdn)
    {
        return preg_match('/([0]{1}[0-9]{10})|((?:[+]{1}44)(?:[0-9]{10}))/', $msisdn);
    }

    public function isProductIdValid(?string $productId)
    {
        return strlen($productId) < 256;
    }
}

