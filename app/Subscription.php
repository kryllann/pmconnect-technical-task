<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    protected $fillable = ['msisdn', 'product_id', 'subscribed_at', 'unsubscribed_at'];
}
