<?php declare(strict_types=1);

namespace App\Services;
use App\Subscription;
use Carbon\Carbon;


class SubscriptionService
{

    /**
     * @param $msisdn
     * @return Subscription|null
     */
    public function find(string $msisdn) : ?array
    {
        return Subscription::where('msisdn', $this->getMsisdnInInternationalForm($msisdn))->get()->toArray();
    }

    /**
     * @param $msisdn
     * @param $productId
     * @return bool
     */
    public function subscribe($msisdn, $productId) : bool
    {
        $subscription = new Subscription(['msisdn' => $this->getMsisdnInInternationalForm($msisdn), 'product_id' => $productId, 'subscribed_at' => Carbon::now()]);
        return $subscription->save();
    }

    /**
     * @param string $msisdn
     * @param string $productId
     * @return bool
     */
    public function unsubscribe(string $msisdn, string $productId = null) : int
    {
        $subscriptions = Subscription::where('msisdn', $this->getMsisdnInInternationalForm($msisdn));
        if ($productId) {
            $subscriptions->where('product_id', $productId);
        }
        return $subscriptions->update(['unsubscribed_at' => Carbon::now()]);
    }

    /**
     * @param string|null $msisdn
     * @return string|null
     */
    public function getMsisdnInInternationalForm(?string $msisdn)
    {
        if (substr($msisdn, 0, 1) === '0') {
            return "+44".substr($msisdn, 1);
        }
        return $msisdn;
    }
}