<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/subscriptions/{msisdn}', 'SubscriptionController@find');

Route::post('/subscriptions/subscribe', 'SubscriptionController@subscribe');
Route::post('/subscriptions/{msisdn}/{product_id}/subscribe', 'SubscriptionController@subscribe');

Route::post('/subscriptions/unsubscribe', 'SubscriptionController@unsubscribe');
Route::post('/subscriptions/{msisdn}/{product_id}/unsubscribe', 'SubscriptionController@unsubscribe');
Route::post('/subscriptions/{msisdn}/unsubscribe', 'SubscriptionController@unsubscribe');