This is Laravel Application, which implements PMSubService. To start: 
1. Clone the repository,
2. Create and fill .env file, as per .env.example
3. Run 'composer install'  
4. Create database with same name as provided in .env file
5. Run 'php artisan migrate'
6. Run 'php artisan serve'

App is now ready to handle the requests with following routes:

GET /api/subscriptions/{msisdn}

POST /api/subscriptions/subscribe
POST /api/subscriptions/{msisdn}/{product_id}/subscribe

POST /api/subscriptions/unsubscribe
POST /api/subscriptions/{msisdn}/{product_id}/unsubscribe
POST /api/subscriptions/{msisdn}/unsubscribe

Parameters accepted:
- msisdn : accepting both formats, and converting to international format before save in Subscribe,
    and	before searching in Find
- product_id : accepting string of maximum 255 characters

Subscribe, Unsubscribe and Find functionality works with additional requirements:
- All endpoints are returning valid JSON.
- Returned subscriptions includes the subscribed and unsubscribed date.
- The product_id parameter on the unsubscribe endpoint is optional.
- When only a msisdn is provided, it unsubscribes from all products.
- All parameters may be provided in the request as a query string, via json, or via x-www-form-urlencoded.


List of files created by me:
app/Http/Controllers/SubscriptionController.php - handling the requests, validation
app/Services/SubscriptionService.php - handling saving to database
app/Traits/SubscriptionValidator.php - helper trait for validation
routes/api.php - contains endpoints


What might be improved:
- All endpoints are returning valid JSON.
- Returned subscriptions includes the subscribed and unsubscribed date.
- The product_id parameter on the unsubscribe endpoint is optional.
- When only a msisdn is provided, it unsubscribes from all products.
- All parameters may be provided in the request as a query string, via json, or via x-www-form-urlencoded.
